﻿namespace uMod.Patcher
{
    partial class TypeMethodPrompt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.detailsgroup = new System.Windows.Forms.GroupBox();
            this.detailstable = new System.Windows.Forms.TableLayoutPanel();
            this.declarationtextbox = new System.Windows.Forms.TextBox();
            this.declarationlabel = new System.Windows.Forms.Label();
            this.typenamelabel = new System.Windows.Forms.Label();
            this.typenametextbox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cancelbutton = new System.Windows.Forms.Button();
            this.selectmethodbutton = new System.Windows.Forms.Button();
            this.splitter = new System.Windows.Forms.SplitContainer();
            this.objectview = new System.Windows.Forms.TreeView();
            this.detailsgroup.SuspendLayout();
            this.detailstable.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitter)).BeginInit();
            this.splitter.Panel1.SuspendLayout();
            this.splitter.SuspendLayout();
            this.SuspendLayout();
            // 
            // detailsgroup
            // 
            this.detailsgroup.Controls.Add(this.detailstable);
            this.detailsgroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.detailsgroup.Location = new System.Drawing.Point(0, 0);
            this.detailsgroup.Name = "detailsgroup";
            this.detailsgroup.Size = new System.Drawing.Size(800, 75);
            this.detailsgroup.TabIndex = 1;
            this.detailsgroup.TabStop = false;
            this.detailsgroup.Text = "Class Details";
            // 
            // detailstable
            // 
            this.detailstable.ColumnCount = 2;
            this.detailstable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 146F));
            this.detailstable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.detailstable.Controls.Add(this.declarationtextbox, 1, 1);
            this.detailstable.Controls.Add(this.declarationlabel, 0, 1);
            this.detailstable.Controls.Add(this.typenamelabel, 0, 0);
            this.detailstable.Controls.Add(this.typenametextbox, 1, 0);
            this.detailstable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailstable.Location = new System.Drawing.Point(3, 16);
            this.detailstable.Name = "detailstable";
            this.detailstable.RowCount = 2;
            this.detailstable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.detailstable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.detailstable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.detailstable.Size = new System.Drawing.Size(794, 56);
            this.detailstable.TabIndex = 0;
            // 
            // declarationtextbox
            // 
            this.declarationtextbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.declarationtextbox.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.declarationtextbox.Location = new System.Drawing.Point(149, 29);
            this.declarationtextbox.Name = "declarationtextbox";
            this.declarationtextbox.ReadOnly = true;
            this.declarationtextbox.Size = new System.Drawing.Size(642, 23);
            this.declarationtextbox.TabIndex = 4;
            // 
            // declarationlabel
            // 
            this.declarationlabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.declarationlabel.Location = new System.Drawing.Point(3, 26);
            this.declarationlabel.Name = "declarationlabel";
            this.declarationlabel.Size = new System.Drawing.Size(140, 30);
            this.declarationlabel.TabIndex = 2;
            this.declarationlabel.Text = "Declaration:";
            this.declarationlabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // typenamelabel
            // 
            this.typenamelabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.typenamelabel.Location = new System.Drawing.Point(3, 0);
            this.typenamelabel.Name = "typenamelabel";
            this.typenamelabel.Size = new System.Drawing.Size(140, 26);
            this.typenamelabel.TabIndex = 0;
            this.typenamelabel.Text = "Fully Qualified Typename:";
            this.typenamelabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // typenametextbox
            // 
            this.typenametextbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.typenametextbox.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.typenametextbox.Location = new System.Drawing.Point(149, 3);
            this.typenametextbox.Name = "typenametextbox";
            this.typenametextbox.ReadOnly = true;
            this.typenametextbox.Size = new System.Drawing.Size(642, 23);
            this.typenametextbox.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cancelbutton);
            this.panel1.Controls.Add(this.selectmethodbutton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 419);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 31);
            this.panel1.TabIndex = 2;
            // 
            // CancelButton
            // 
            this.cancelbutton.Dock = System.Windows.Forms.DockStyle.Left;
            this.cancelbutton.Location = new System.Drawing.Point(104, 0);
            this.cancelbutton.Name = "CancelButton";
            this.cancelbutton.Size = new System.Drawing.Size(104, 31);
            this.cancelbutton.TabIndex = 1;
            this.cancelbutton.Text = "Cancel";
            this.cancelbutton.UseVisualStyleBackColor = true;
            this.cancelbutton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // SelectMethodButton
            // 
            this.selectmethodbutton.Dock = System.Windows.Forms.DockStyle.Left;
            this.selectmethodbutton.Location = new System.Drawing.Point(0, 0);
            this.selectmethodbutton.Name = "SelectMethodButton";
            this.selectmethodbutton.Size = new System.Drawing.Size(104, 31);
            this.selectmethodbutton.TabIndex = 0;
            this.selectmethodbutton.Text = "Select";
            this.selectmethodbutton.UseVisualStyleBackColor = true;
            this.selectmethodbutton.Click += new System.EventHandler(this.SelectMethodButton_Click);
            // 
            // splitter
            // 
            this.splitter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitter.Location = new System.Drawing.Point(0, 75);
            this.splitter.Name = "splitter";
            // 
            // splitter.Panel1
            // 
            this.splitter.Panel1.Controls.Add(this.objectview);
            this.splitter.Size = new System.Drawing.Size(800, 344);
            this.splitter.SplitterDistance = 265;
            this.splitter.TabIndex = 3;
            // 
            // objectview
            // 
            this.objectview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectview.Location = new System.Drawing.Point(0, 0);
            this.objectview.Name = "objectview";
            this.objectview.Size = new System.Drawing.Size(265, 344);
            this.objectview.TabIndex = 0;
            this.objectview.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.objectview_AfterSelect);
            // 
            // TypeMethodPrompt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ControlBox = false;
            this.Controls.Add(this.splitter);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.detailsgroup);
            this.Name = "TypeMethodPrompt";
            this.Text = "Select Method";
            this.Load += new System.EventHandler(this.TypeMethodPrompt_Load);
            this.detailsgroup.ResumeLayout(false);
            this.detailstable.ResumeLayout(false);
            this.detailstable.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.splitter.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitter)).EndInit();
            this.splitter.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.GroupBox detailsgroup;
        private System.Windows.Forms.TableLayoutPanel detailstable;
        private System.Windows.Forms.TextBox declarationtextbox;
        private System.Windows.Forms.Label declarationlabel;
        private System.Windows.Forms.Label typenamelabel;
        private System.Windows.Forms.TextBox typenametextbox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cancelbutton;
        private System.Windows.Forms.Button selectmethodbutton;
        private System.Windows.Forms.SplitContainer splitter;
        private System.Windows.Forms.TreeView objectview;
    }
}
