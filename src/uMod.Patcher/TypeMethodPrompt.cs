﻿using System;
using System.Windows.Forms;
using Mono.Cecil;

namespace uMod.Patcher
{
    public partial class TypeMethodPrompt : Form
    {
        public TypeDefinition TypeDef { get; set; }

        public PatcherForm MainForm { get; set; }

        public MethodDefinition SelectedMethodDefinition { get; set; }

        public PropertyDefinition SelectedPropertyDefinition { get; set; }

        public FieldDefinition SelectedFieldDefinition { get; set; }

        public Action<TypeMethodPrompt> OnMemberSelected = null;

        public FormUtility.MemberFilter MemberFilter = FormUtility.MemberFilter.All;

        private Control currentview;

        public TypeMethodPrompt()
        {
            InitializeComponent();
        }

        private void TypeMethodPrompt_Load(object sender, System.EventArgs e)
        {
            FormUtility.PopulateDetails(TypeDef, typenametextbox, declarationtextbox);

            FormUtility.PopulateTree(TypeDef, objectview, MemberFilter);
        }

        private void CancelButton_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void SelectMethodButton_Click(object sender, System.EventArgs e)
        {
            OnMemberSelected?.Invoke(this);
            Close();
        }

        private void objectview_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (currentview != null)
            {
                splitter.Panel2.Controls.Remove(currentview);
                currentview.Dispose();
                currentview = null;
            }

            TreeNode selected = e.Node;
            if (selected == null)
            {
                return;
            }

            switch (selected.Tag)
            {
                case MethodDefinition tag:
                {
                    MethodViewControl methodview = new MethodViewControl
                    {
                        Dock = DockStyle.Fill,
                        MethodDef = tag,
                        MainForm = MainForm,
                        MemberActions = false
                    };
                    splitter.Panel2.Controls.Add(methodview);
                    currentview = methodview;
                    SelectedMethodDefinition = tag;
                    break;
                }
                case PropertyDefinition _:
                case FieldDefinition _:
                {
                    FieldAndPropertyViewControl fieldpropertyview = new FieldAndPropertyViewControl
                    {
                        Dock = DockStyle.Fill,
                        PropertyDef = selected.Tag as PropertyDefinition,
                        FieldDef = selected.Tag as FieldDefinition,
                        MainForm = MainForm,
                        MemberActions = false
                    };

                    SelectedFieldDefinition = selected.Tag as FieldDefinition;
                    SelectedPropertyDefinition = selected.Tag as PropertyDefinition;
                    splitter.Panel2.Controls.Add(fieldpropertyview);
                    currentview = fieldpropertyview;
                    break;
                }
            }
        }
    }
}
