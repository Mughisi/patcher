﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;
using static System.Windows.Forms.Application;

namespace uMod.Patcher
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

            AppDomain.CurrentDomain.AssemblyResolve += (_, args1) =>
            {
                string resourceName = "uMod.Patcher.Dependencies." + new AssemblyName(args1.Name).Name + ".dll";
                if (resourceName.Contains("resources.dll"))
                {
                    return null;
                }

                using Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
                if (stream != null)
                {
                    byte[] assemblyData = new byte[stream.Length];
                    stream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData);
                }

                return null;
            };

            EnableVisualStyles();
            SetCompatibleTextRenderingDefault(false);
            Run(new PatcherForm());
        }
    }
}
