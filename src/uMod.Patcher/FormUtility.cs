﻿using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace uMod.Patcher
{
    public static class FormUtility
    {
        internal static void PopulateDetails(TypeDefinition typeDef, TextBox typenametextbox, TextBox declarationtextbox)
        {
            try
            {
                typenametextbox.Text = typeDef.FullName;
                StringBuilder sb = new StringBuilder();
                sb.Append(typeDef.IsPublic ? "public " : "private ");

                if (typeDef.IsSealed)
                {
                    sb.Append("sealed ");
                }

                sb.Append("class ");
                sb.Append(typeDef.Name);
                if (Utility.TransformType(typeDef.BaseType.Name) != "object")
                {
                    sb.AppendFormat(" : {0} ", typeDef.BaseType.Name);
                }

                declarationtextbox.Text = sb.ToString();
            }
            catch (NullReferenceException)
            {
                MessageBox.Show(PatcherForm.MainForm, "Error loading details for a class. It may be empty.",
                    "Null Reference Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        [Flags]
        public enum MemberFilter
        {
            All = 0,
            Methods = 1,
            Fields = 2,
            Properties = 4
        }

        internal static void PopulateTree(TypeDefinition typeDef, TreeView objectview, MemberFilter filter = MemberFilter.All)
        {
            // Clear tree
            objectview.Nodes.Clear();

            // Create root nodes
            TreeNode staticnode = new TreeNode("Static Members");
            TreeNode staticnodeFields = new TreeNode("Fields");
            TreeNode staticnodeProperties = new TreeNode("Properties");
            TreeNode staticnodeMethods = new TreeNode("Methods");
            TreeNode instancenode = new TreeNode("Instance Members");
            TreeNode instancenodeFields = new TreeNode("Fields");
            TreeNode instancenodeProperties = new TreeNode("Properties");
            TreeNode instancenodeMethods = new TreeNode("Methods");

            // Get all members and sort
            FieldDefinition[] fielddefs = typeDef.Fields.ToArray();
            Array.Sort(fielddefs, (a, b) => Comparer<string>.Default.Compare(a.Name, b.Name));
            PropertyDefinition[] propdefs = typeDef.Properties.ToArray();
            Array.Sort(propdefs, (a, b) => Comparer<string>.Default.Compare(a.Name, b.Name));
            MethodDefinition[] methoddefs = typeDef.Methods.ToArray();
            Array.Sort(methoddefs, (a, b) => Comparer<string>.Default.Compare(a.Name, b.Name));

            // Add fields
            if (filter.HasFlag(MemberFilter.Fields) || filter == MemberFilter.All)
            {
                for (int i = 0; i < fielddefs.Length; i++)
                {
                    FieldDefinition field = fielddefs[i];
                    string qualifier = field.IsPublic ? "public" : field.IsPrivate ? "private" : "protected";
                    string name = $"{qualifier} {Utility.TransformType(field.FieldType.Name)} {field.Name}";
                    TreeNode node = new TreeNode(name);
                    string icon = SelectIcon(field);
                    node.ImageKey = icon;
                    node.SelectedImageKey = icon;
                    node.Tag = field;
                    if (field.IsStatic)
                    {
                        staticnodeFields.Nodes.Add(node);
                    }
                    else
                    {
                        instancenodeFields.Nodes.Add(node);
                    }
                }
            }

            HashSet<MethodDefinition> ignoremethods = new HashSet<MethodDefinition>();
            // Add properties
            if (filter.HasFlag(MemberFilter.Properties) || filter == MemberFilter.All)
            {
                for (int i = 0; i < propdefs.Length; i++)
                {
                    PropertyDefinition prop = propdefs[i];
                    StringBuilder sb = new StringBuilder();
                    sb.Append("(");
                    if (prop.GetMethod != null)
                    {
                        ignoremethods.Add(prop.GetMethod);
                        if (prop.GetMethod.IsPrivate)
                        {
                            sb.Append("private ");
                        }
                        else if (prop.GetMethod.IsPublic)
                        {
                            sb.Append("public ");
                        }
                        else
                        {
                            sb.Append("protected ");
                        }

                        sb.Append("getter, ");
                    }

                    if (prop.SetMethod != null)
                    {
                        ignoremethods.Add(prop.SetMethod);
                        if (prop.SetMethod.IsPrivate)
                        {
                            sb.Append("private ");
                        }
                        else if (prop.SetMethod.IsPublic)
                        {
                            sb.Append("public ");
                        }
                        else
                        {
                            sb.Append("protected ");
                        }

                        sb.Append("setter");
                    }

                    sb.Append(")");
                    string name = $"{Utility.TransformType(prop.PropertyType.Name)} {prop.Name} {sb}";
                    TreeNode node = new TreeNode(name);
                    string icon = SelectIcon(prop);
                    node.ImageKey = icon;
                    node.SelectedImageKey = icon;
                    node.Tag = prop;
                    bool propstatic = (prop.GetMethod != null && prop.GetMethod.IsStatic) ||
                                      (prop.SetMethod != null && prop.SetMethod.IsStatic);
                    if (propstatic)
                    {
                        staticnodeProperties.Nodes.Add(node);
                    }
                    else
                    {
                        instancenodeProperties.Nodes.Add(node);
                    }
                }
            }

            // Add methods
            if (filter.HasFlag(MemberFilter.Methods) || filter == MemberFilter.All)
            {
                for (int i = 0; i < methoddefs.Length; i++)
                {
                    MethodDefinition method = methoddefs[i];
                    if (!ignoremethods.Contains(method))
                    {
                        string name = Utility.GetMethodDeclaration(method);
                        TreeNode node = new TreeNode(name);
                        string icon = SelectIcon(method);
                        node.ImageKey = icon;
                        node.SelectedImageKey = icon;
                        node.Tag = method;
                        if (method.IsStatic)
                        {
                            staticnodeMethods.Nodes.Add(node);
                        }
                        else
                        {
                            instancenodeMethods.Nodes.Add(node);
                        }
                    }
                }
            }

            // Add all nodes
            if (instancenodeFields.Nodes.Count > 0)
            {
                instancenode.Nodes.Add(instancenodeFields);
            }

            if (instancenodeProperties.Nodes.Count > 0)
            {
                instancenode.Nodes.Add(instancenodeProperties);
            }

            if (instancenodeMethods.Nodes.Count > 0)
            {
                instancenode.Nodes.Add(instancenodeMethods);
            }

            if (instancenode.Nodes.Count > 0)
            {
                objectview.Nodes.Add(instancenode);
            }

            if (staticnodeFields.Nodes.Count > 0)
            {
                staticnode.Nodes.Add(staticnodeFields);
            }

            if (staticnodeProperties.Nodes.Count > 0)
            {
                staticnode.Nodes.Add(staticnodeProperties);
            }

            if (staticnodeMethods.Nodes.Count > 0)
            {
                staticnode.Nodes.Add(staticnodeMethods);
            }

            if (staticnode.Nodes.Count > 0)
            {
                objectview.Nodes.Add(staticnode);
            }

            instancenode.Expand();
            instancenodeMethods.Expand();
            staticnode.Expand();
            staticnodeMethods.Expand();
        }

        private static string SelectIcon(FieldDefinition field)
        {
            if (field.IsPrivate)
            {
                return "Field-Private_545.png";
            }

            if (field.IsPublic)
            {
                return "FieldIcon.png";
            }

            return "Field-Protected_544.png";
        }

        private static string SelectIcon(PropertyDefinition prop)
        {
            MethodDefinition getmethod = prop.GetMethod;
            MethodDefinition setmethod = prop.SetMethod;
            if (getmethod == null && setmethod == null)
            {
                return "Property_501.png";
            }

            if (getmethod != null && getmethod.IsPublic || setmethod != null && setmethod.IsPublic)
            {
                return "Property_501.png";
            }

            return "Property-Private_505.png";
        }

        private static string SelectIcon(MethodDefinition method)
        {
            if (method.IsPrivate)
            {
                return "Method-Private_640.png";
            }

            if (method.IsPublic)
            {
                return "Method_636.png";
            }

            return "Method-Protected_639.png";
        }
    }
}
