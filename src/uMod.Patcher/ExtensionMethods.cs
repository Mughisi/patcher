﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace uMod.Patcher
{
    internal static class ExtensionMethods
    {
        /// <summary>
        /// Return the nodes in this collection and their descendants.
        /// </summary>
        /// <param name="nodes"></param>
        /// <returns></returns>
        public static IEnumerable<TreeNode> Descendants(
            this TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                yield return node;
                foreach (TreeNode child in node.Nodes.Descendants())
                {
                    yield return child;
                }
            }
        }

        /// <summary>
        /// Return this node and its descendants.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static IEnumerable<TreeNode> Descendants(this TreeNode node)
        {
            yield return node;
            foreach (TreeNode child in node.Nodes.Descendants())
            {
                yield return child;
            }
        }
    }
}
