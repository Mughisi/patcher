﻿using uMod.Patcher.Hooks;

namespace uMod.Patcher.Views
{
    public partial class InitializeHookSettingsControl : HookSettingsControl
    {
        private bool ignorechanges;

        public InitializeHookSettingsControl()
        {
            InitializeComponent();
        }

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            Initialize hook = Hook as Initialize;

            ignorechanges = true;
            injectionindex.Value = hook.InjectionIndex;
            ignorechanges = false;
        }

        private void injectionindex_ValueChanged(object sender, System.EventArgs e)
        {
            if (ignorechanges)
            {
                return;
            }

            Initialize hook = Hook as Initialize;
            hook.InjectionIndex = (int)injectionindex.Value;
            NotifyChanges();
        }

        public override void UpdateCaptureGroups()
        {
        }
    }
}
