﻿using ICSharpCode.TextEditor;
using ICSharpCode.TextEditor.Document;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using uMod.Patcher.Exceptions;
using uMod.Patcher.Hooks;
using uMod.Patcher.Patching;

namespace uMod.Patcher.Views
{
    public partial class HookViewControl : UserControl
    {
        /// <summary>
        /// Gets or sets the hook to use
        /// </summary>
        public Hook Hook { get; set; }

        /// <summary>
        /// Gets or sets the main patcher form
        /// </summary>
        public PatcherForm MainForm { get; set; }

        public Button FlagButton { get; set; }

        public Button UnflagButton { get; set; }

        private List<Type> hooktypes;

        private TextEditorControl msilbefore, msilafter, codebefore, codeafter;

        private MethodDefinition methoddef;

        private bool ignoretypechange;

        private CaptureGroupsControl capturegroups;

        private HookSettingsControl settingsview;

        public HookViewControl()
        {
            InitializeComponent();
            FlagButton = flagbutton;
            UnflagButton = unflagbutton;
        }

        protected override async void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            methoddef = MainForm.GetMethod(Hook.AssemblyName, Hook.TypeName, Hook.Signature);

            hooktypes = new List<Type>();
            int selindex = 0;
            int i = 0;
            foreach (Type hooktype in Hook.GetHookTypes())
            {
                string typename = hooktype.GetCustomAttribute<HookType>().Name;
                hooktypedropdown.Items.Add(typename);
                hooktypes.Add(hooktype);
                if (typename == Hook.HookTypeName)
                {
                    selindex = i;
                }

                i++;
            }

            List<Hook> hooks = Project.Current.GetManifest(Hook.AssemblyName).Hooks;
            List<Hook> baseHooks = (from hook in hooks where hook.BaseHook != null select hook.BaseHook).ToList();
            basehookdropdown.Items.Add("");
            int selindex2 = 0;
            i = 1;
            foreach (Hook hook in hooks)
            {
                if (hook.BaseHook == Hook)
                {
                    clonebutton.Enabled = false;
                }

                if (hook != Hook.BaseHook && baseHooks.Contains(hook))
                {
                    continue;
                }

                basehookdropdown.Items.Add(hook.Name);
                if (hook == Hook.BaseHook)
                {
                    selindex2 = i;
                }

                i++;
            }

            if (!string.IsNullOrEmpty(Hook.BaseHookName))
            {
                baseHookSearchButton.Enabled = true;
            }
            else
            {
                baseHookSearchButton.Enabled = false;
            }

            assemblytextbox.Text = Hook.AssemblyName;
            typenametextbox.Text = Hook.TypeName;

            if (methoddef != null)
            {
                methodnametextbox.Text = Hook.Signature.ToString();
            }
            else
            {
                methodnametextbox.Text = Hook.Signature + " (METHOD MISSING)";
            }

            nametextbox.Text = Hook.Name;
            hooknametextbox.Text = Hook.HookName;
            ignoretypechange = true;
            hooktypedropdown.SelectedIndex = selindex;
            basehookdropdown.SelectedIndex = selindex2;
            ignoretypechange = false;

            applybutton.Enabled = false;

            if (Hook.Flagged)
            {
                flagbutton.Enabled = false;
                unflagbutton.Enabled = true;
                unflagbutton.Focus();
            }
            else
            {
                flagbutton.Enabled = true;
                unflagbutton.Enabled = false;
                flagbutton.Focus();
            }

            switch (Hook)
            {
                case Initialize initialize:
                    settingsview = new InitializeHookSettingsControl { Hook = initialize };
                    tabview.TabPages.Remove(capturegroupstab);
                    break;

                case Pattern patternHook:
                    settingsview = new PatternHookSettingsControl { Hook = patternHook };
                    break;

                case Simple simpleHook:
                    settingsview = new SimpleHookSettingsControl { Hook = simpleHook };
                    tabview.TabPages.Remove(capturegroupstab);
                    break;

                case Modify modifyHook:
                    settingsview = new ModifyHookSettingsControl { Hook = modifyHook };
                    tabview.TabPages.Remove(capturegroupstab);
                    break;
            }

            if (settingsview == null)
            {
                Label tmp = new Label
                {
                    TextAlign = ContentAlignment.MiddleCenter,
                    AutoSize = false,
                    Text = "No settings.",
                    Dock = DockStyle.Fill
                };
                hooksettingstab.Controls.Add(tmp);
            }
            else
            {
                settingsview.Dock = DockStyle.Fill;
                settingsview.OnSettingsChanged += settingsview_OnSettingsChanged;
                hooksettingstab.Controls.Add(settingsview);
            }

            capturegroups = new CaptureGroupsControl()
            {
                Dock = DockStyle.Fill,
                Hook = Hook
            };

            capturegroups.OnSettingsChanged += capturegroups_OnSettingsChanged;

            capturegroupstab.Controls.Add(capturegroups);
            UpdateCaptureGroups();

            if (methoddef == null)
            {
                Label missinglabel1 = new Label();
                missinglabel1.Dock = DockStyle.Fill;
                missinglabel1.AutoSize = false;
                missinglabel1.Text = "METHOD MISSING";
                missinglabel1.TextAlign = ContentAlignment.MiddleCenter;
                beforetab.Controls.Add(missinglabel1);

                Label missinglabel2 = new Label();
                missinglabel2.Dock = DockStyle.Fill;
                missinglabel2.AutoSize = false;
                missinglabel2.Text = "METHOD MISSING";
                missinglabel2.TextAlign = ContentAlignment.MiddleCenter;
                aftertab.Controls.Add(missinglabel2);

                return;
            }

            IlWeaver weaver = new IlWeaver(methoddef.Body) { Module = methoddef.Module };

            Font font = new Font(FontFamily.GenericMonospace, 10.0f);

            Hook.PreparePatch(methoddef, weaver, MainForm.CoreAssembly);
            msilbefore = new TextEditorControl
            {
                Font = font,
                Dock = DockStyle.Fill,
                Text = weaver.ToString(),
                IsReadOnly = true
            };
            codebefore = new TextEditorControl
            {
                Font = font,
                Dock = DockStyle.Fill,
                Text = await Decompiler.GetSourceCode(methoddef, weaver),
                Document = { HighlightingStrategy = HighlightingManager.Manager.FindHighlighter("C#") },
                IsReadOnly = true
            };

            try
            {
                Hook.ApplyPatch(methoddef, weaver, MainForm.CoreAssembly);
                MainForm?.ClearStatus();
            }
            catch (PatchInvalidException ex)
            {
                MainForm?.SetError($"{ex.Header}: {ex.Text}");
                //MessageBox.Show(ex.Text, ex.Header, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            msilafter = new TextEditorControl
            {
                Font = font,
                Dock = DockStyle.Fill,
                Text = weaver.ToString(),
                IsReadOnly = true
            };
            codeafter = new TextEditorControl
            {
                Font = font,
                Dock = DockStyle.Fill,
                Text = await Decompiler.GetSourceCode(methoddef, weaver),
                Document = { HighlightingStrategy = HighlightingManager.Manager.FindHighlighter("C#") },
                IsReadOnly = true
            };

            msilbefore.ActiveTextAreaControl.TextArea.MouseClick += TextAreaOnMouseClick;

            beforetab.Controls.Add(msilbefore);
            aftertab.Controls.Add(msilafter);
            codebeforetab.Controls.Add(codebefore);
            codeaftertab.Controls.Add(codeafter);
        }

        private void ContextMenuStripOnItemClicked(Pattern hook, CaptureGroup captureGroup = null)
        {
            if (hook.CaptureGroups == null)
            {
                hook.CaptureGroups = new CaptureGroup[0];
            }
            List<CaptureGroup> captureGroupList = new List<CaptureGroup>(hook.CaptureGroups);

            if (captureGroup == null)
            {
                captureGroupList.Add(captureGroup = new CaptureGroup()
                {
                    Name = "Group" + (captureGroupList.Count + 1),
                });
                hook.CaptureGroups = captureGroupList.ToArray();
            }

            if (!msilbefore.ActiveTextAreaControl.SelectionManager.HasSomethingSelected)
            {
                return;
            }

            ISelection selection = msilbefore.ActiveTextAreaControl.SelectionManager.SelectionCollection.FirstOrDefault();
            if (selection == null)
            {
                return;
            }

            IlWeaver weaver = new IlWeaver(methoddef.Body) { Module = methoddef.Module };
            if (captureGroup.Patterns == null)
            {
                captureGroup.Patterns = new string[0];
            }

            int start = selection.StartPosition.Line;
            int end = selection.EndPosition.Line;

            List<string> patterns = new List<string>(captureGroup.Patterns);

            for (int line = start; line <= end; line++)
            {
                Instruction instruction = weaver.Instructions[line];
                string instructionStr;
                if (instruction.OpCode.FlowControl is FlowControl.Next or FlowControl.Call or FlowControl.Return)
                {
                    int instructionOffsetLength = $"IL_{instruction.Offset:X4} :".Length;
                    instructionStr = instruction.ToString()[instructionOffsetLength..];
                }
                else
                {
                    instructionStr = instruction.OpCode.Name;
                }
                patterns.Add(instructionStr);
            }

            captureGroup.Patterns = patterns.ToArray();
            UpdateCaptureGroups();
        }

        public void UpdateCaptureGroups()
        {
            capturegroups.UpdateCaptureGroups();
            settingsview.UpdateCaptureGroups();
        }

        private void TextAreaOnMouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && Hook is Pattern patternHook)
            {
                InitializeContextMenuStrip();
                contextMenuStrip1.Show(msilbefore.ActiveTextAreaControl.TextArea, e.Location);
            }
        }

        private void InitializeContextMenuStrip()
        {
            if (Hook is Pattern patternHook)
            {
                ToolStripItem toolItem;
                contextMenuStrip1.Items.Clear();
                if (patternHook.CaptureGroups != null)
                {
                    foreach (CaptureGroup group in patternHook.CaptureGroups)
                    {
                        toolItem = contextMenuStrip1.Items.Add("Add to " + group.Name);
                        toolItem.Click += delegate { ContextMenuStripOnItemClicked(patternHook, group); };
                    }
                }

                toolItem = contextMenuStrip1.Items.Add("Add to new group");
                toolItem.Click += delegate
                {
                    ContextMenuStripOnItemClicked(patternHook);
                };
            }
        }

        private void capturegroups_OnSettingsChanged(HookSettingsControl obj)
        {
            UpdateCaptureGroups();
            applybutton.Enabled = true;
        }

        private void settingsview_OnSettingsChanged(HookSettingsControl obj)
        {
            applybutton.Enabled = true;
        }

        private void deletebutton_Click(object sender, System.EventArgs e)
        {
            DialogResult result = MessageBox.Show(MainForm, "Are you sure you want to remove this hook?", "uMod Patcher", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                MainForm.RemoveHook(Hook);
            }
        }

        private void flagbutton_Click(object sender, System.EventArgs e)
        {
            Hook.Flagged = true;
            MainForm.UpdateHook(Hook, false);
            flagbutton.Enabled = false;
            unflagbutton.Enabled = true;
        }

        private void unflagbutton_Click(object sender, System.EventArgs e)
        {
            Hook.Flagged = false;
            MainForm.UpdateHook(Hook, false);
            if (Hook.Flagged)
            {
                return;
            }

            flagbutton.Enabled = true;
            unflagbutton.Enabled = false;
        }

        private void hooktypedropdown_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (hooktypedropdown.SelectedIndex < 0)
            {
                return;
            }

            if (ignoretypechange)
            {
                return;
            }

            Type t = hooktypes[hooktypedropdown.SelectedIndex];
            if (t == null)
            {
                return;
            }

            DialogResult result = MessageBox.Show(MainForm, "Are you sure you want to change the type of this hook? Any hook settings will be lost.", "uMod Patcher", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                MainForm.RemoveHook(Hook);
                Hook newhook = Activator.CreateInstance(t) as Hook;
                newhook.Name = Hook.Name;
                newhook.HookName = Hook.HookName;
                newhook.AssemblyName = Hook.AssemblyName;
                newhook.TypeName = Hook.TypeName;
                newhook.Signature = Hook.Signature;
                newhook.Flagged = Hook.Flagged;
                newhook.MsilHash = Hook.MsilHash;
                newhook.BaseHook = Hook.BaseHook;
                newhook.BaseHookName = Hook.BaseHookName;
                newhook.HookCategory = Hook.HookCategory;
                MainForm.AddHook(newhook);
                MainForm.GotoHook(newhook);
            }
        }

        private void nametextbox_TextChanged(object sender, System.EventArgs e)
        {
            applybutton.Enabled = true;
        }

        private void hooknametextbox_TextChanged(object sender, System.EventArgs e)
        {
            applybutton.Enabled = true;
        }

        private void typeSearchButton_Click(object sender, EventArgs e)
        {
            TypeDefinition typeDef = MainForm?.GetTypeDefinition(Hook.TypeName);
            if (typeDef != null)
            {
                MainForm?.GotoType(typeDef);
                return;
            }

            MainForm?.Search(Hook.TypeName);
        }

        private void baseHookSearchButton_Click(object sender, EventArgs e)
        {
            Hook hookTarget = MainForm?.GetHook(Hook.BaseHookName);
            if (hookTarget != null)
            {
                MainForm?.GotoHook(Hook.BaseHook);
                return;
            }

            MainForm?.Search(Hook.BaseHookName);
        }

        private void MethodSelectButton_Click(object sender, EventArgs e)
        {
            TypeDefinition typeDef = MainForm?.GetTypeDefinition(Hook.TypeName);

            if (typeDef == null)
            {
                MainForm?.SetError($"No type found: {Hook.TypeName}");
                return;
            }

            TypeMethodPrompt typeMethodPrompt = new TypeMethodPrompt
            {
                StartPosition = FormStartPosition.CenterParent,
                MainForm = MainForm,
                TypeDef = typeDef,
                OnMemberSelected = MethodSelected,
                MemberFilter = FormUtility.MemberFilter.Methods
            };
            typeMethodPrompt.ShowDialog(this);
        }

        private void MethodSelected(TypeMethodPrompt prompt)
        {
            Hook.Signature = Utility.GetMethodSignature(prompt.SelectedMethodDefinition);
            methodnametextbox.Text = Hook.Signature.ToString();
            Apply();
        }

        private void applybutton_Click(object sender, System.EventArgs e)
        {
            Apply();
        }

        private async void Apply()
        {
            Hook.Name = nametextbox.Text;
            Hook.HookName = hooknametextbox.Text;

            MainForm.UpdateHook(Hook, false);

            if (msilbefore != null && msilafter != null)
            {
                IlWeaver weaver = new IlWeaver(methoddef.Body) { Module = methoddef.Module };

                Hook.PreparePatch(methoddef, weaver, MainForm.CoreAssembly);
                msilbefore.Text = weaver.ToString();
                codebefore.Text = await Decompiler.GetSourceCode(methoddef, weaver);

                try
                {
                    Hook.ApplyPatch(methoddef, weaver, MainForm.CoreAssembly);
                }
                catch (PatchInvalidException ex)
                {
                    MainForm?.SetError($"{ex.Header}: {ex.Text}");
                    //MessageBox.Show(ex.Text, ex.Header, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                msilafter.Text = weaver.ToString();
                codeafter.Text = await Decompiler.GetSourceCode(methoddef, weaver);
            }

            applybutton.Enabled = false;
        }

        private void basehookdropdown_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (basehookdropdown.SelectedIndex < 0)
            {
                return;
            }

            if (ignoretypechange)
            {
                return;
            }

            string hookName = (string)basehookdropdown.SelectedItem;
            if (string.IsNullOrWhiteSpace(hookName))
            {
                Hook.BaseHook = null;
                return;
            }
            List<Hook> hooks = Project.Current.GetManifest(Hook.AssemblyName).Hooks;
            foreach (Hook hook in hooks)
            {
                if (hook.Name.Equals(hookName))
                {
                    Hook.BaseHook = hook;
                    break;
                }
            }
            if (!Hook.BaseHook.Name.Equals(hookName))
            {
                MessageBox.Show(MainForm, "Base Hook not found!", "uMod Patcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void clonebutton_Click(object sender, System.EventArgs e)
        {
            Hook newhook = Activator.CreateInstance(Hook.GetType()) as Hook;
            newhook.Name = Hook.Name + "(Clone)";
            newhook.HookName = Hook.HookName + "(Clone)";
            newhook.AssemblyName = Hook.AssemblyName;
            newhook.TypeName = Hook.TypeName;
            newhook.Signature = Hook.Signature;
            newhook.Flagged = Hook.Flagged;
            newhook.MsilHash = Hook.MsilHash;
            newhook.BaseHook = Hook;
            MainForm.AddHook(newhook);
            MainForm.GotoHook(newhook);
            clonebutton.Enabled = false;
        }
    }
}
