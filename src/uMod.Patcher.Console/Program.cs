using Mono.Options;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using uMod.Patcher.Hooks;

namespace uMod.Patcher
{
    internal class Program
    {
        // Defines for command-line output
        [DllImport("kernel32.dll")]
        private static extern bool AttachConsole(int dwProcessId);

        private const int AttachParentProcess = -1;

        /// <summary>
        /// The main entry point for the application
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

            AppDomain.CurrentDomain.AssemblyResolve += (_, args1) =>
            {
                string resourceName = $"uMod.Patcher.Dependencies.{new AssemblyName(args1.Name).Name}.dll";
                if (resourceName.Contains("resources.dll"))
                {
                    return null;
                }

                using Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
                if (stream != null)
                {
                    byte[] assemblyData = new byte[stream.Length];
                    stream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData);
                }

                return null;
            };

            string filename = "Game.opj";
            bool unflagAll = false;
            string targetOverride = string.Empty;
            string error = string.Empty;
            string platform = "both";
            HookOperatingSystem hookPlatform = HookOperatingSystem.All;

            OptionSet optionSet = new()
            {
                { "<>", "the .opj file", value => { filename = value; } },
                { "u|unflag", "whether to unflag all hooks", _ => { unflagAll = true; } },
                {
                    "d|dir=",
                    "the target directory",
                    value =>
                    {
                        if (!Directory.Exists(value))
                        {
                            throw new OptionException($"Directory ({value}) not found", "dir");
                        }
                        targetOverride = value;
                    }
                },
                { "p|platform=", "the platform to patch for", value => { platform = value; } },
            };

            try
            {
                optionSet.Parse(args);
            }
            catch (OptionException oEx)
            {
                Console.WriteLine($"{oEx.OptionName}: {oEx.Message}");
            }

            hookPlatform = platform.ToLower() switch
            {
                "both" => HookOperatingSystem.All,
                "windows" => HookOperatingSystem.Windows,
                "linux" => HookOperatingSystem.Linux,
                _ => hookPlatform
            };

            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                case PlatformID.WinCE:
                    // Redirect console output to parent process; must be before any calls to Console.WriteLine()
                    AttachConsole(AttachParentProcess);
                    break;
            }

            if (error != "")
            {
                Console.WriteLine($"ERROR: {error}");
                return;
            }

            if (!Directory.Exists(targetOverride) && targetOverride != "")
            {
                Console.WriteLine($"{targetOverride} does not exist!");
                return;
            }

            if (!File.Exists(filename))
            {
                Console.WriteLine($"{filename} does not exist!");
                return;
            }

            Project patchProject = targetOverride == "" ? Project.Load(filename) : Project.Load(filename, targetOverride);

            if (string.IsNullOrEmpty(patchProject.TargetDirectory))
            {
                patchProject.TargetDirectory = Environment.CurrentDirectory;
            }

            if (unflagAll)
            {
                Unflag(patchProject, filename, true);
            }

            patchProject.Platform = hookPlatform;

            Patching.Patcher patcher = new(patchProject, true, delegate (string header, string message)
            {
                Console.WriteLine($"{header}: {message}");
            });

            patcher.Patch();
        }

        private static void Unflag(Project project, string filename, bool console)
        {
            bool updated = false;
            foreach (Hook hook in project.Manifests.SelectMany(m => m.Hooks))
            {
                if (hook.Flagged)
                {
                    hook.Flagged = false;
                    updated = true;
                    if (console)
                    {
                        Console.WriteLine($"Hook {hook.HookName} has been unflagged");
                    }
                }
            }
            if (updated)
            {
                project.Save(filename);
            }
        }
    }
}
