﻿using Mono.Cecil;
using System;
using System.IO;

namespace uMod.Patcher.Patching
{
    /// <summary>
    /// Allows Mono.Cecil to locate assemblies when trying to build references
    /// </summary>
    public class AssemblyResolver : IAssemblyResolver
    {
        /// <summary>
        /// Gets or sets the target directory
        /// </summary>
        public string[] Directories { get; set; }

        // The fallback resolver
        private readonly DefaultAssemblyResolver fallback;

        private readonly ReaderParameters readerParameters;

        /// <summary>
        /// Initializes a new instance of the AssemblyResolver class
        /// </summary>
        public AssemblyResolver()
        {
            fallback = new DefaultAssemblyResolver();
            readerParameters = new ReaderParameters()
            {
                AssemblyResolver = this
            };
        }

        private static readonly string[] CommaSplit = { ", " };

        private AssemblyDefinition LocalResolve(string fullName)
        {
            foreach (string targetDirectory in Directories)
            {
                if (string.IsNullOrEmpty(targetDirectory))
                {
                    continue;
                }

                string[] data = fullName.Split(CommaSplit, StringSplitOptions.RemoveEmptyEntries);
                string filename = Path.Combine(targetDirectory, data[0] + ".dll");
                if (!File.Exists(filename))
                {
                    filename = Path.Combine(targetDirectory, data[0] + ".exe");
                }

                if (File.Exists(filename))
                {
                    return AssemblyDefinition.ReadAssembly(filename, readerParameters);
                }
            }

            return null;
        }

        public AssemblyDefinition Resolve(string fullName, ReaderParameters parameters)
        {
            AssemblyDefinition result = LocalResolve(fullName) ?? fallback.Resolve(AssemblyNameReference.Parse(fullName), parameters);
            return result;
        }

        public AssemblyDefinition Resolve(string fullName)
        {
            AssemblyDefinition result = LocalResolve(fullName) ?? fallback.Resolve(AssemblyNameReference.Parse(fullName));
            return result;
        }

        public AssemblyDefinition Resolve(AssemblyNameReference name, ReaderParameters parameters)
        {
            AssemblyDefinition result = LocalResolve(name.FullName) ?? fallback.Resolve(name, parameters);
            return result;
        }

        public AssemblyDefinition Resolve(AssemblyNameReference name)
        {
            AssemblyDefinition result = LocalResolve(name.FullName) ?? fallback.Resolve(name);
            return result;
        }

        public void Dispose()
        {
        }
    }
}
