﻿using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using uMod.Patcher.Patching;

namespace uMod.Patcher.Deobfuscation
{
    /// <summary>
    /// A deobfuscator capable of deobfuscating assemblies obfuscated by CodeGuard for Unity
    /// </summary>
    public class UnityCodeGuard : Deobfuscator
    {
        /// <summary>
        /// Gets the name of this deobfuscator
        /// </summary>
        public override string Name => "Unity CodeGuard";

        /// <summary>
        /// Gets the priority of this deobfuscator
        /// </summary>
        public override int Priority => 0;

        // The current reference count map
        private Dictionary<MethodReference, int> refcounts;

        /// <summary>
        /// Gets all relevant types for the specified assembly
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        private static IEnumerable<TypeDefinition> GetTypes(AssemblyDefinition assembly)
        {
            //TypeReference objectref = assembly.MainModule.TypeSystem.Object;
            return assembly.MainModule.Types;
            /*.Where((t) =>
                {
                    TypeDefinition curbase = t;
                    while (curbase != objectref)
                    {
                        if (curbase.BaseType == null) return false;
                        try
                        {
                            curbase = curbase.BaseType.Resolve();
                        }
                        catch (AssemblyResolutionException)
                        {
                            return false;
                        }
                        if (curbase.FullName == "UnityEngine.MonoBehaviour")
                            return true;
                    }
                    return false;
                })*/
        }

        /// <summary>
        /// Returns if this deobfuscator is capable of deobfuscating the specified assembly
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public override bool CanDeobfuscate(AssemblyDefinition assembly)
        {
            // Search the assembly for field and method names which have been made into symbols
            // Look through types that inherit MonoBehaviour at some point in the inheritance chain
            int count = 0;
            //TypeReference objectReference = assembly.MainModule.TypeSystem.Object;
            foreach (TypeDefinition type in GetTypes(assembly))
            {
                // Search all members
                foreach (MethodDefinition method in type.Methods)
                {
                    if (IdentifyObfuscatedName(method.Name))
                    {
                        count++;
                    }
                }

                foreach (FieldDefinition field in type.Fields)
                {
                    if (IdentifyObfuscatedName(field.Name))
                    {
                        count++;
                    }
                }

                foreach (PropertyDefinition property in type.Properties)
                {
                    if (IdentifyObfuscatedName(property.Name))
                    {
                        count++;
                    }
                }
            }

            // If we have more than a certain number of obfuscated methods, return true
            // We do this to allow for a few false positives
            const int threshold = 5;
            return count >= threshold;
        }

        /// <summary>
        /// Returns if the specified variable, field or method name has been obfuscated or not
        /// </summary>
        /// <param name="obfuscatedName"></param>
        /// <returns></returns>
        private static bool IdentifyObfuscatedName(string obfuscatedName)
        {
            // Symbols obfuscated by CodeGuard consist of a single unicode character
            if (obfuscatedName.Length != 1)
            {
                return false;
            }

            char c = obfuscatedName[0];

            // "Normal" names are anything from a-z, A-Z or an underscore
            // These names might still be obfuscated but probably not by CodeGuard

            return c switch
            {
                >= 'a' and <= 'z' or >= 'A' and <= 'Z' or '_' => false,
                _ => true // CodeGuard probably renamed this
            };
        }

        /// <summary>
        /// Deobfuscates the specified assembly, returning a success value
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public override bool Deobfuscate(AssemblyDefinition assembly)
        {
            // Build the refmap
            refcounts = new Dictionary<MethodReference, int>();
            foreach (Instruction inst in GetTypes(assembly).SelectMany(t => t.Methods).Where(m => m.HasBody).SelectMany(m => m.Body.Instructions))
            {
                // Get method
                if (!(inst.Operand is MethodReference method))
                {
                    continue;
                }
                // Increment refcounter
                if (refcounts.TryGetValue(method, out int curcount))
                {
                    refcounts[method] = curcount + 1;
                }
                else
                {
                    refcounts.Add(method, 1);
                }
            }

            // Search the assembly for field and method names which have been made into symbols
            // Look through types that inherit MonoBehaviour at some point in the inheritance chain
            foreach (TypeDefinition type in GetTypes(assembly))
            {
                // Deobfuscate
                DeobfuscateType(type);
            }
            return true;
        }

        /// <summary>
        /// Attempts to deobfuscate the specified type
        /// </summary>
        /// <param name="typedef"></param>
        protected virtual void DeobfuscateType(TypeDefinition typedef)
        {
            // Deal with method parameters
            foreach (MethodDefinition method in typedef.Methods.Where(m => m.HasParameters))
            {
                for (int i = 0; i < method.Parameters.Count; i++)
                {
                    ParameterDefinition paramDefinition = method.Parameters[i];
                    if (IdentifyObfuscatedName(paramDefinition.Name))
                    {
                        string name = $"arg{i}";
                        paramDefinition.Name = name;
                    }
                }
            }

            // Deal with field names
            FieldDefinition[] fields = typedef.Fields.Where(f => IdentifyObfuscatedName(f.Name)).ToArray();
            Array.Sort(fields, (a, b) =>
            {
                // Sort firstly by type
                // Then sort by offset
                // Finally, sort by obfuscated name
                int tmp = Comparer<string>.Default.Compare(a.FieldType.FullName, b.FieldType.FullName);
                if (tmp != 0)
                {
                    return tmp;
                }

                tmp = Comparer<int>.Default.Compare(a.Offset, b.Offset);
                return tmp != 0 ? tmp : Comparer<string>.Default.Compare(a.Name, b.Name);
            });

            for (int i = 0; i < fields.Length; i++)
            {
                FieldDefinition field = fields[i];
                string name = field.IsPublic ? $"Field{i + 1}" : $"field{i + 1}";
                field.Name = name;
            }

            // Deal with property names
            PropertyDefinition[] properties = typedef.Properties.Where(f => IdentifyObfuscatedName(f.Name)).ToArray();
            Array.Sort(properties, (a, b) =>
            {
                // Sort firstly by type, then by obfuscated name
                int tmp = Comparer<string>.Default.Compare(a.PropertyType.FullName, b.PropertyType.FullName);
                return tmp != 0 ? tmp : Comparer<string>.Default.Compare(a.Name, b.Name);
            });

            for (int i = 0; i < properties.Length; i++)
            {
                PropertyDefinition property = properties[i];
                string name = $"property{i + 1}";
                // NOTE: Do we need to rename the get and set methods too?
                property.Name = name;
            }

            // Deal with method names
            MethodDefinition[] methods = typedef.Methods.Where(f => IdentifyObfuscatedName(f.Name)).ToArray();
            Array.Sort(methods, (a, b) =>
            {
                // Sort by the following in order: return type, parameter count, parameter types, obfuscated name
                int tmp = Comparer<string>.Default.Compare(a.ReturnType.FullName, b.ReturnType.FullName);
                if (tmp != 0)
                {
                    return tmp;
                }

                tmp = Comparer<int>.Default.Compare(a.Parameters.Count, b.Parameters.Count);
                return tmp != 0 ? tmp : Comparer<string>.Default.Compare(a.Name, b.Name); // TODO: Sort by parameter types
            });

            for (int i = 0; i < methods.Length; i++)
            {
                MethodDefinition method = methods[i];
                string name = method.IsPublic ? $"Method{i + 1}" : $"method{i + 1}";
                method.Name = name;
            }

            // Deal with proxy methods
            HashSet<MethodDefinition> toremove = new();
            foreach (MethodDefinition method in typedef.Methods.Where(m => m.HasBody))
            {
                // Identify a proxy call via IL
                Collection<Instruction> instructions = method.Body.Instructions;
                if (instructions.Count != 3)
                {
                    continue;
                }

                if (instructions[0].OpCode.Code != Code.Ldarg_0)
                {
                    continue;
                }

                if (instructions[1].OpCode.Code != Code.Callvirt)
                {
                    continue;
                }

                if (instructions[2].OpCode.Code != Code.Ret)
                {
                    continue;
                }

                // Check that it is calling an obfuscated method in our type
                MethodReference proxyMethod = instructions[1].Operand as MethodReference;
                if (proxyMethod.DeclaringType.FullName != typedef.FullName)
                {
                    continue;
                }

                if (methods.All(m => m.FullName != proxyMethod.FullName))
                {
                    continue;
                }

                // Check that the target method is not referenced by anything else
                if (!refcounts.TryGetValue(proxyMethod, out int refcount))
                {
                    refcount = 0;
                }

                if (refcount > 1)
                {
                    continue;
                }

                // Resolve it
                MethodDefinition proxyMethodDefinition = proxyMethod.Resolve();
                if (!proxyMethodDefinition.HasBody)
                {
                    continue;
                }

                // It passed, collapse the proxy method's IL into this method and remove it
                IlWeaver weaver = new(proxyMethodDefinition.Body);
                weaver.Apply(method.Body);
                toremove.Add(proxyMethodDefinition);
            }

            // Remove any proxy methods
            foreach (MethodDefinition method in toremove)
            {
                typedef.Methods.Remove(method);
            }
        }
    }
}
