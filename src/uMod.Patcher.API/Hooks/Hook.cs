﻿using Mono.Cecil;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using uMod.Patcher.Exceptions;
using uMod.Patcher.Patching;

namespace uMod.Patcher.Hooks
{
    public enum MethodExposure { Private, Protected, Public, Internal }

    /// <summary>
    /// Indicates details about the hook type for use with UI
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public sealed class HookType : Attribute
    {
        /// <summary>
        /// Gets a human-friendly name for this hook type
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets if this hook type should be used as the default
        /// </summary>
        public bool Default { get; set; }

        public HookType(string name)
        {
            Name = name;
        }
    }

    /// <summary>
    /// Represents the signature of a method
    /// </summary>
    public sealed class MethodSignature
    {
        /// <summary>
        /// Gets the method exposure
        /// </summary>
        public MethodExposure Exposure { get; }

        /// <summary>
        /// Gets the method name
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the method return type as a fully qualified type name
        /// </summary>
        public string ReturnType { get; }

        /// <summary>
        /// Gets the parameter list as fully qualified type names
        /// </summary>
        public string[] Parameters { get; }

        /// <summary>
        /// Initializes a new instance of the MethodSignature class
        /// </summary>
        /// <param name="exposure"></param>
        /// <param name="returnType"></param>
        /// <param name="name"></param>
        /// <param name="parameters"></param>
        public MethodSignature(MethodExposure exposure, string returnType, string name, string[] parameters)
        {
            Exposure = exposure;
            ReturnType = returnType;
            Name = name;
            Parameters = parameters;
        }

        public override bool Equals(object obj)
        {
            if (obj is not MethodSignature otherSignature)
            {
                return false;
            }

            if (Exposure != otherSignature.Exposure || Name != otherSignature.Name)
            {
                return false;
            }

            if (Parameters.Length != otherSignature.Parameters.Length)
            {
                return false;
            }

            for (int i = 0; i < Parameters.Length; i++)
            {
                if (Parameters[i] != otherSignature.Parameters[i])
                {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            int total = Exposure.GetHashCode() + Name.GetHashCode();
            foreach (string param in Parameters)
            {
                total += param.GetHashCode();
            }

            return total;
        }

        public override string ToString()
        {
            StringBuilder sb = new();
            sb.Append($"{Exposure.ToString().ToLower()} {Utility.TransformType(ReturnType)} {Name}(");
            for (int i = 0; i < Parameters.Length; i++)
            {
                if (i > 0)
                {
                    sb.Append($", {Parameters[i]}");
                }
                else
                {
                    sb.Append(Parameters[i]);
                }
            }

            sb.Append(')');
            return sb.ToString();
        }
    }

    public enum HookOperatingSystem
    {
        All = 0,
        Windows = 1,
        Linux = 2
    }

    /// <summary>
    /// Represents a hook that is applied to single method and calls a single hook
    /// </summary>
    public abstract class Hook
    {
        /// <summary>
        /// Gets a human friendly type name for this hook
        /// </summary>
        public string HookTypeName => GetType().GetCustomAttribute<HookType>()?.Name;

        /// <summary>
        /// Gets or sets a name for this hook
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the hook to call
        /// </summary>
        public string HookName { get; set; }

        /// <summary>
        /// Gets or sets the name of the assembly in which the target type resides
        /// </summary>
        public string AssemblyName { get; set; }

        /// <summary>
        /// Gets or sets the fully qualified name for the type in which the target method resides
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets if this hook has been flagged
        /// </summary>
        [DefaultValue(false)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool Flagged { get; set; }

        /// <summary>
        /// Gets or sets the target method signature
        /// </summary>
        public MethodSignature Signature { get; set; }

        /// <summary>
        /// Gets or sets the MSIL hash of the target method
        /// </summary>
        public string MsilHash { get; set; }

        /// <summary>
        /// Gets or sets the base hook name
        /// </summary>
        [DefaultValue("")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string BaseHookName { get; set; }

        /// <summary>
        /// Gets or sets the operating system
        /// </summary>
        [DefaultValue(HookOperatingSystem.All)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public HookOperatingSystem OperatingSystem { get; set; } = HookOperatingSystem.All;

        /// <summary>
        /// Gets or sets the base hook
        /// </summary>
        [JsonIgnore]
        public Hook BaseHook { get; set; }

        /// <summary>
        /// Gets or sets the hook category
        /// </summary>
        public string HookCategory { get; set; }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(HookCategory))
            {
                return $"{HookCategory}/{Name} (Hook)";
            }

            return Name;
        }

        protected static void ShowMsg(string msg, string header, Patching.Patcher patcher)
        {
            if (patcher != null)
            {
                patcher.Log(msg);
            }
            else
            {
                throw new HookInvalidException(header, msg);
            }
        }

        /// <summary>
        /// PrePatches this hook into the target weaver
        /// </summary>
        /// <param name="weaver"></param>
        /// <param name="module"></param>
        /// <param name="original"></param>
        /// <param name="patcher"></param>
        public bool PreparePatch(MethodDefinition original, IlWeaver weaver, AssemblyDefinition module, Patching.Patcher patcher = null)
        {
            if (BaseHook != null)
            {
                return BaseHook.PreparePatch(original, weaver, module, patcher) && BaseHook.ApplyPatch(original, weaver, module, patcher);
            }

            return true;
        }

        /// <summary>
        /// Patch hook using the specified method into the target weaver
        /// </summary>
        /// <param name="original"></param>
        /// <param name="weaver"></param>
        /// <param name="moduleAssembly"></param>
        /// <param name="patcher"></param>
        public abstract bool ApplyPatch(MethodDefinition original, IlWeaver weaver, AssemblyDefinition moduleAssembly, Patching.Patcher patcher = null);

        #region Static Interface

        private static readonly Type[] HookTypes;
        private static readonly Type DefaultHookType;

        static Hook()
        {
            Type baseType = typeof(Hook);
            HookTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(GetAllTypesFromAssembly)
                .Where(t => !t.IsAbstract)
                .Where(t => baseType.IsAssignableFrom(t))
                .Where(t => t.GetCustomAttribute<HookType>() != null)
                .ToArray();
            foreach (Type hookType in HookTypes)
            {
                HookType type = hookType.GetCustomAttribute<HookType>();
                if (type is { Default: true })
                {
                    DefaultHookType = hookType;
                    break;
                }
            }
        }

        /// <summary>
        /// Gets all hook types
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Type> GetHookTypes()
        {
            return HookTypes;
        }

        /// <summary>
        /// Gets a hook type by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Type GetHookType(string name)
        {
            return HookTypes.FirstOrDefault(type => type.Name == name);
        }

        /// <summary>
        /// Gets the default hook type
        /// </summary>
        /// <returns></returns>
        public static Type GetDefaultHookType()
        {
            return DefaultHookType;
        }

        #endregion Static Interface

        private static IEnumerable<Type> GetAllTypesFromAssembly(Assembly asm)
        {
            foreach (Module module in asm.GetModules())
            {
                Type[] moduleTypes;
                try
                {
                    moduleTypes = module.GetTypes();
                }
                catch (ReflectionTypeLoadException e)
                {
                    moduleTypes = e.Types;
                }
                catch (Exception)
                {
                    moduleTypes = Type.EmptyTypes;
                }

                foreach (Type type in moduleTypes)
                {
                    if (type != null)
                    {
                        yield return type;
                    }
                }
            }
        }
    }
}
