﻿using Mono.Cecil;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using uMod.Patcher.Exceptions;
using uMod.Patcher.Hooks;
using uMod.Patcher.Patching;

namespace uMod.Patcher
{
    /// <summary>
    /// An uMod patcher project
    /// </summary>
    public class Project
    {
        public static Project Current;

        [JsonIgnore]
        public HookOperatingSystem Platform { get; set; } = HookOperatingSystem.All;

        /// <summary>
        /// Gets or sets the project name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the directory of the DLLs
        /// </summary>
        public string TargetDirectory { get; set; }

        /// <summary>
        /// Gets or sets all the manifests contained in this project
        /// </summary>
        public List<Manifest> Manifests { get; set; }

        /// <summary>
        /// Gets or sets the core path
        /// </summary>
        [JsonIgnore]
        public string CorePath { get; set; }

        /// <summary>
        /// Initializes a new instance of the Project class with sensible defaults
        /// </summary>
        public Project()
        {
            // Fill in defaults
            Name = "Untitled Project";
            TargetDirectory = "";
            Manifests = new List<Manifest>();
        }

        /// <summary>
        /// Saves this project to file
        /// </summary>
        public void Save(string filename)
        {
            File.WriteAllText(filename, JsonConvert.SerializeObject(this, Formatting.Indented));
        }

        /// <summary>
        /// Loads this project from file
        /// </summary>
        /// <returns></returns>
        public static Project Load(string filename)
        {
            if (!File.Exists(filename))
            {
                return new Project();
            }

            string text = File.ReadAllText(filename);
            try
            {
                return JsonConvert.DeserializeObject<Project>(text);
            }
            catch (JsonReaderException ex)
            {
                throw new ProjectInvalidException("There was a problem loading the project file!", "Are all file paths properly escaped?", ex);
            }
        }

        public static Project Load(string filename, string overrideTarget)
        {
            if (!File.Exists(filename))
            {
                return new Project();
            }

            string text = File.ReadAllText(filename);
            try
            {
                Project project = JsonConvert.DeserializeObject<Project>(text);
                if (project != null)
                {
                    project.TargetDirectory = overrideTarget;
                    return project;
                }

                return null;
            }
            catch (JsonReaderException ex)
            {
                throw new ProjectInvalidException("There was a problem loading the project file!", "Are all file paths properly escaped?", ex);
            }
        }

        /// <summary>
        /// Adds an empty manifest with the given assembly name to the project
        /// </summary>
        /// <param name="assemblyName"></param>
        public void AddManifest(string assemblyName)
        {
            Manifest manifest = new() { AssemblyName = assemblyName };
            Manifests.Add(manifest);
        }

        /// <summary>
        /// Removes the manifest that references the specified assembly name from the project
        /// </summary>
        /// <param name="assemblyName"></param>
        public void RemoveManifest(string assemblyName)
        {
            Manifest manifest = GetManifest(assemblyName);
            if (manifest != null)
            {
                Manifests.Remove(manifest);
            }
        }

        /// <summary>
        /// Gets the manifest with the specified assembly name
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public Manifest GetManifest(string assemblyName)
        {
            foreach (Manifest manifest in Manifests)
            {
                if (manifest.AssemblyName == assemblyName)
                {
                    return manifest;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the assembly definition from the specified assembly path
        /// </summary>
        /// <param name="path"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public AssemblyDefinition GetAssembly(string path, ReaderParameters parameters = null)
        {
            string fileName = Path.GetFileName(path);

            if (!File.Exists(path))
            {
                if (fileName == "Oxide.Core.dll" || fileName == "uMod.Core.dll")
                {
                    path = CorePath;
                }
                else if (!string.IsNullOrEmpty(CorePath) && !string.IsNullOrEmpty(fileName))
                {
                    string corePathDirectory = Path.GetDirectoryName(CorePath);
                    if (corePathDirectory != null)
                    {
                        string possiblePath = Path.Combine(corePathDirectory, fileName);
                        if (File.Exists(possiblePath))
                        {
                            path = possiblePath;
                        }
                    }
                }
            }

            if (!File.Exists(path))
            {
                throw new FileNotFoundException($"Failed to locate {fileName} assembly");
            }

            return parameters == null ? AssemblyDefinition.ReadAssembly(path) : AssemblyDefinition.ReadAssembly(path, parameters);
        }

        public AssemblyResolver GetAssemblyResolver(string target = null)
        {
            target ??= TargetDirectory;

            string[] directories = {
                target
            };

            if (!string.IsNullOrEmpty(CorePath))
            {
                string corePathDir = Path.GetDirectoryName(CorePath);
                if (corePathDir != target)
                {
                    directories = new[]
                    {
                        target,
                        corePathDir
                    };
                }
            }

            return new AssemblyResolver
            {
                Directories = directories
            };
        }
    }
}
