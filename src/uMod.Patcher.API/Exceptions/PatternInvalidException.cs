﻿using System;

namespace uMod.Patcher.Exceptions
{
    public class PatternInvalidException : PatchInvalidException
    {
        public PatternInvalidException(string header, string message, Exception innerException = null) : base(header, message, innerException)
        {
        }
    }
}
