﻿using System;

namespace uMod.Patcher.Exceptions
{
    public class HookInvalidException : PatchInvalidException
    {
        public HookInvalidException(string header, string message, Exception innerException = null) : base(header, message, innerException)
        {
        }
    }
}
