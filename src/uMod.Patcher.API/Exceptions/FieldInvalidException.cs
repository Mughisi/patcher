﻿using System;

namespace uMod.Patcher.Exceptions
{
    public class FieldInvalidException : PatchInvalidException
    {
        public FieldInvalidException(string header, string message, Exception innerException = null) : base(header, message, innerException)
        {
        }
    }
}
