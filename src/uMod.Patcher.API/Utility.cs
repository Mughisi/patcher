﻿using Mono.Cecil;
using System;
using System.Linq;
using uMod.Patcher.Hooks;
using uMod.Patcher.Modifiers;

namespace uMod.Patcher
{
    /// <summary>
    /// Contains helpful utility methods
    /// </summary>
    public static class Utility
    {
        /// <summary>
        /// Transforms the specified type name into a more human readable name
        /// </summary>
        /// <param name="oldTypeName"></param>
        /// <returns></returns>
        public static string TransformType(string oldTypeName)
        {
            const string prefix = "System.";
            if (oldTypeName.Length >= prefix.Length && oldTypeName[..prefix.Length] == prefix)
            {
                string smallerType = oldTypeName[prefix.Length..];
                string newType = TransformType(smallerType);
                return newType == smallerType ? oldTypeName : newType;
            }

            return oldTypeName switch
            {
                "String" => "string",
                "Integer" => "int",
                "Boolean" => "bool",
                "Object" => "object",
                "UInt16" => "ushort",
                "UInt32" => "uint",
                "UInt64" => "ulong",
                "Int16" => "short",
                "Int32" => "int",
                "Int64" => "long",
                "Byte" => "byte",
                "Void" => "void",
                "Single" => "float",
                "Double" => "double",
                _ => oldTypeName
            };
        }

        /// <summary>
        /// Gets the C# qualifier string for the specified method
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public static string GetMethodQualifier(MethodDefinition method)
        {
            string qualifier;
            if (method.IsStatic)
            {
                qualifier = method.IsPublic ? "public static" : method.IsPrivate ? "private static" : "internal static";
            }
            else if (method.IsAbstract)
            {
                qualifier = method.IsPublic ? "public abstract" : method.IsPrivate ? "private abstract" : method.IsFamilyOrAssembly || method.IsFamily ? "protected abstract" : "internal abstract";
            }
            else if (method.IsVirtual)
            {
                qualifier = method.IsPublic ? "public virtual" : method.IsPrivate ? "private virtual" : method.IsFamilyOrAssembly || method.IsFamily ? "protected virtual" : "internal virtual";
            }
            else
            {
                qualifier = method.IsPublic ? "public" : method.IsPrivate ? "private" : "protected";
            }

            return qualifier;
        }

        /// <summary>
        /// Gets the C# method string for the specified method
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public static string GetMethodDeclaration(MethodDefinition method)
        {
            string name;
            string qualifier = GetMethodQualifier(method);
            string[] args = method.Parameters.Select(x => $"{TransformType(x.ParameterType.Name)} {x.Name}").ToArray();
            if (method.Name == ".ctor" || method.Name == ".cctor")
            {
                name = $"{qualifier} {method.DeclaringType.Name}({string.Join(", ", args)})";
            }
            else
            {
                name = $"{qualifier} {TransformType(method.ReturnType.Name)} {method.Name}({string.Join(", ", args)})";
            }

            return name;
        }

        /// <summary>
        /// Gets a method signature for the specified method
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public static MethodSignature GetMethodSignature(MethodDefinition method)
        {
            MethodExposure exposure;
            if (method.IsPublic)
            {
                exposure = MethodExposure.Public;
            }
            else if (method.IsPrivate)
            {
                exposure = MethodExposure.Private;
            }
            else if (method.IsFamilyOrAssembly || method.IsFamily)
            {
                exposure = MethodExposure.Protected;
            }
            else
            {
                exposure = MethodExposure.Internal;
            }

            string[] parameters = new string[method.Parameters.Count];
            for (int i = 0; i < parameters.Length; i++)
            {
                parameters[i] = method.Parameters[i].ParameterType.FullName;
            }
            return new MethodSignature(exposure, method.ReturnType.FullName, method.Name, parameters);
        }

        /// <summary>
        /// Gets a signature for the specified field
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public static ModifierSignature GetModifierSignature(FieldDefinition field)
        {
            Exposure exposure;
            if (field.IsPublic)
            {
                exposure = Exposure.Public;
            }
            else if (field.IsPrivate)
            {
                exposure = Exposure.Private;
            }
            else if (field.IsFamilyOrAssembly || field.IsFamily)
            {
                exposure = Exposure.Protected;
            }
            else
            {
                exposure = Exposure.Internal;
            }

            return new ModifierSignature(exposure, field.FullName, field.Name, Array.Empty<string>());
        }

        /// <summary>
        /// Gets a signature for the specified method
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public static ModifierSignature GetModifierSignature(MethodDefinition method)
        {
            Exposure exposure;
            if (method.IsPublic)
            {
                exposure = Exposure.Public;
            }
            else if (method.IsPrivate)
            {
                exposure = Exposure.Private;
            }
            else if (method.IsFamilyOrAssembly || method.IsFamily)
            {
                exposure = Exposure.Protected;
            }
            else
            {
                exposure = Exposure.Internal;
            }

            string[] parameters = new string[method.Parameters.Count];
            for (int i = 0; i < parameters.Length; i++)
            {
                parameters[i] = method.Parameters[i].ParameterType.FullName;
            }

            return new ModifierSignature(exposure, method.ReturnType.FullName, method.Name, parameters);
        }

        /// <summary>
        /// Gets a signature for the specified property
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public static ModifierSignature GetModifierSignature(PropertyDefinition property)
        {
            Exposure getExposure = Exposure.Null;
            Exposure setExposure = Exposure.Null;

            if (property.GetMethod != null)
            {
                if (property.GetMethod.IsPublic)
                {
                    getExposure = Exposure.Public;
                }
                else if (property.GetMethod.IsPrivate)
                {
                    getExposure = Exposure.Private;
                }
                else if (property.GetMethod.IsFamilyOrAssembly || property.GetMethod.IsFamily)
                {
                    getExposure = Exposure.Protected;
                }
                else
                {
                    getExposure = Exposure.Protected;
                }
            }

            if (property.SetMethod != null)
            {
                if (property.SetMethod.IsPublic)
                {
                    setExposure = Exposure.Public;
                }
                else if (property.SetMethod.IsPrivate)
                {
                    setExposure = Exposure.Private;
                }
                else if (property.SetMethod.IsFamilyOrAssembly || property.SetMethod.IsFamily)
                {
                    setExposure = Exposure.Protected;
                }
                else
                {
                    setExposure = Exposure.Protected;
                }
            }

            return new ModifierSignature(new[] { getExposure, setExposure }, property.FullName, property.Name, Array.Empty<string>());
        }

        /// <summary>
        /// Gets a signature for the specified type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static ModifierSignature GetModifierSignature(TypeDefinition type)
        {
            Exposure exposure = Exposure.Null;
            if (type.IsPublic || type.IsNestedPublic)
            {
                exposure = Exposure.Public;
            }
            else if (type.IsNotPublic || type.IsNestedPrivate)
            {
                exposure = Exposure.Private;
            }

            return new ModifierSignature(exposure, type.FullName, type.Name, Array.Empty<string>());
        }
    }
}
